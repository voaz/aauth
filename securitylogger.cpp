#include "securitylogger.h"

SecurityLogger* SecurityLogger::instance = nullptr;

SecurityLogger *SecurityLogger::getInstance() {
    if (instance) {
        return instance;
    } else {
        instance = new SecurityLogger();
        return instance;
    }
}

void SecurityLogger::setLogFile(const QString logFilePath) {
    this->logFilePath = logFilePath;
}

void SecurityLogger::writeLog(const QString message) {
    std::ofstream logstream(logFilePath.toStdString(), std::ios_base::app);

    if (!logstream) {
        //обработчик
    }
    logstream << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ").toStdString()
              << std::string(" : ")
              << message.toStdString()
              << std::string("\n");

}
