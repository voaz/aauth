#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QLabel>
#include <QCheckBox>
#include <QPalette>

#include "authenticator.h"
#include "securitylogger.h"

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    void setAuthFilePath(QString path);
    ~MainWindow();


private:
    void initSuccessWidget();
    void initLoginWidget();

    QPushButton*    submitButton;
    QCheckBox*      passwordVisibilityCheckBox;
    QLineEdit*      passwordInput;
    QLineEdit*      loginInput;
    QWidget*        logingWidget;
    QWidget*        successWidget;
    QLabel*         info;

    QString         authFilePath;
    Authenticator*  authenticator;
    UserMod         usermod;
    int             triesCount;
private slots:
    void onSubmitButtonClicked();
    void onPassVisibilityChange();
    void dataInputFocused();
    void exitButtonClicked();
};

#endif // MAINWINDOW_H
