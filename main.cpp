#include "mainwindow.h"
#include <QApplication>

#include "securitylogger.h"

//Дефайны путей до файлов
#define LOG_FILE_PATH "./logfile.txt"
#define AUTH_FILE_PATH "./auth.txt"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    SecurityLogger::getInstance()->setLogFile(LOG_FILE_PATH);

    MainWindow w;
    w.setAuthFilePath(AUTH_FILE_PATH);
    w.resize(300,200);
    w.show();

    return a.exec();
}
