#-------------------------------------------------
#
# Project created by QtCreator 2017-01-30T12:37:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Aauth
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    authenticator.cpp \
    securitylogger.cpp

HEADERS  += mainwindow.h \
    authenticator.h \
    securitylogger.h
