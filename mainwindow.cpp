#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    triesCount = 0;
    initLoginWidget();
}


void MainWindow::onSubmitButtonClicked() {
    //проверка данных авторизации

    const QString login = loginInput->text();
    const QString password = passwordInput->text();
    if (login.isEmpty() || password.isEmpty()) {
        SecurityLogger::getInstance()->writeLog("Failed to log in");
        info->setText("Login or password are empty");
        return;
    }
    UserMod mod = authenticator->checkAuthData(login, password);
    if (mod != UserMod::UNKNOWN) {
        //logingWidget->setVisible(false);
        usermod = mod;
        SecurityLogger::getInstance()->writeLog("Log in success");
        initSuccessWidget();
    } else {
        SecurityLogger::getInstance()->writeLog("Failed to log in");
        info->setText("Login or password are incorrect");
        triesCount++;
    }
    if (triesCount >= 3) {
        exit(0);
    }
}

void MainWindow::onPassVisibilityChange() {
    if (passwordInput->echoMode() == QLineEdit::Normal) {
        passwordInput->setEchoMode(QLineEdit::Password);
    } else {
        passwordInput->setEchoMode(QLineEdit::Normal);
    }
}

void MainWindow::setAuthFilePath(QString path) {
    authFilePath = path;
    authenticator = new Authenticator(authFilePath);
}

void MainWindow::initLoginWidget() {
    logingWidget = new QWidget(this);
    QGridLayout* loginFormLayout = new QGridLayout;

    loginFormLayout->addWidget(new QLabel("Login", logingWidget), 0, 0);

    loginInput = new QLineEdit(logingWidget);
    loginFormLayout->addWidget(loginInput, 1, 0, 1, 0);

    loginFormLayout->addWidget(new QLabel("Password", logingWidget), 2, 0);

    passwordInput = new QLineEdit(logingWidget);
    passwordInput->setEchoMode(QLineEdit::Password);
    loginFormLayout->addWidget(passwordInput, 3, 0, 1, 0);

    passwordVisibilityCheckBox = new QCheckBox("Show password", logingWidget);
    connect(passwordVisibilityCheckBox,
            SIGNAL(stateChanged(int)),
            this,
            SLOT(onPassVisibilityChange()));
    loginFormLayout->addWidget(passwordVisibilityCheckBox, 4, 0, 2, 1);

    submitButton = new QPushButton("&OK", logingWidget);
    connect(submitButton,
            SIGNAL(clicked(bool)),
            this,
            SLOT(onSubmitButtonClicked()));
    loginFormLayout->addWidget(submitButton, 5, 1, 2, 1);

    info = new QLabel("", logingWidget);
    loginFormLayout->addWidget(info, 6, 0, 2, 0);

    logingWidget->setLayout(loginFormLayout);
    setCentralWidget(logingWidget);
}

void MainWindow::initSuccessWidget() {
    delete logingWidget;
    QString role = "";
    switch (usermod) {
    case UserMod::ADMINISTRATOR:
        role = "Administrator";
        break;
    case UserMod::MODERATOR:
        role = "Moderator";
        break;
    case UserMod::OPERATOR:
        role = "Operator";
        break;
    default:
        break;
    }
    successWidget = new QWidget(this);
    QVBoxLayout* successLayout = new QVBoxLayout(successWidget);
    successLayout->addWidget(
                new QLabel("Welcome, " + loginInput->text()
                            + "!\nYour role is " + role, successWidget));
    successLayout->addStretch(1);
    QPushButton* exitButton = new QPushButton("&Exit", successWidget);
    connect(exitButton, SIGNAL(clicked(bool)), this,  SLOT(exitButtonClicked()));
    successLayout->addWidget(exitButton);
    setCentralWidget(successWidget);
}



MainWindow::~MainWindow() {
    delete authenticator;
    delete successWidget;
}


void MainWindow::dataInputFocused() {
    info->setText("");
}

void MainWindow::exitButtonClicked() {
    authenticator->writeToLogCounter();
    this->close();
}

