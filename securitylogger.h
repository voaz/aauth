#ifndef SECURITYLOGGER_H
#define SECURITYLOGGER_H

#include <QString>
#include <fstream>
#include <QDateTime>

//Класс реализован как синглтон. Так его удобно использовать как логер.
class SecurityLogger {
public:
    //метод для получения указателя на экземпляр класса
    static SecurityLogger* getInstance();
    //метод для инициализации пути до файла логов
    void setLogFile(const QString logFilePath);
    //Запись в лог
    void writeLog(const QString message);

private:
    //Скрываем конструкторы для предотвращения создания экземпляров
    SecurityLogger() {}
    SecurityLogger(const SecurityLogger&);
    SecurityLogger& operator =(SecurityLogger&);

    //указатель на экземпляр класса
    static SecurityLogger* instance;
    //путь до лог-файла
    QString logFilePath;

};

#endif // SECURITYLOGGER_H
