#include "authenticator.h"


Authenticator::Authenticator(const QString pathToAuthFile) : pathToAuthFile(pathToAuthFile){

}

UserMod Authenticator::checkAuthData(const QString login, const QString password) {
    if (pathToAuthFile.isEmpty()) {
        SecurityLogger::getInstance()->writeLog("Can\'t open users file-database. Empty path.");
        return UserMod::UNKNOWN;
    }
    authFStream.open(pathToAuthFile.toStdString());
    if (!authFStream.is_open()) {
        SecurityLogger::getInstance()->writeLog("Can\'t open users file-database");
        return UserMod::UNKNOWN;
    }
    increaseLoggingCounter(login);
    std::string temp = "";
    QString loginData;
    while (std::getline(authFStream, temp)) {
        loginData = QString(temp.c_str());



        int loginIndex = loginData.indexOf(login);
        if (loginIndex != -1) {
            int passwordIndex = loginData.indexOf(password, loginIndex + 1);

            if (    passwordIndex != -1 &&
                    passwordIndex - loginIndex == login.size() + 1 &&
                    loginData.indexOf(";", passwordIndex + password.length()) == passwordIndex + password.length()) {

                SecurityLogger::getInstance()->writeLog("User with login \"" + login + "\" successfully logged in");
                authFStream.close();
                return getUserMod(loginData, passwordIndex + password.length());
            } else {
                SecurityLogger::getInstance()->writeLog("User with login \"" + login + "\" found but password is incorrect");
                authFStream.close();
                return UserMod::UNKNOWN;
            }
        }
    }
    authFStream.close();
    return UserMod::UNKNOWN;
}

UserMod Authenticator::getUserMod(const QString data,const int startIndex) {
    if (data.indexOf("a", startIndex) != -1) {
        return UserMod::ADMINISTRATOR;
    }
    if (data.indexOf("m", startIndex) != -1) {
        return UserMod::MODERATOR;
    }
    if (data.indexOf("o", startIndex) != -1) {
        return UserMod::OPERATOR;
    }
    return UserMod::UNKNOWN;
}

void Authenticator::increaseLoggingCounter(const QString login) {
    if(loggingCounter.contains(login)) {
        loggingCounter[login]++;
    } else {
        loggingCounter.insert(login, 1);
    }
}

void Authenticator::writeToLogCounter() {
    QString output = "";
    for (auto it = loggingCounter.begin(); it != loggingCounter.end(); it++) {
        output += "User with login \"" + it.key() + "\" attempt to log in " + QString::fromStdString(std::to_string(it.value())) + " times\n";
    }
    SecurityLogger::getInstance()->writeLog(output);
}
