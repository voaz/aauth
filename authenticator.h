#ifndef AUTHENTICATOR_H
#define AUTHENTICATOR_H

#include <QString>
#include <fstream>
#include <QPair>
#include <QMap>

#include "securitylogger.h"

enum class UserMod {ADMINISTRATOR, MODERATOR, OPERATOR, UNKNOWN};

class Authenticator {
public:
    Authenticator(QString pathToAuthFile);
    //проверка логина и пароля в базе
    UserMod checkAuthData(QString login, QString password);

    void writeToLogCounter();

    ~Authenticator(){}
private:
    Authenticator();
    //метод увеличения счетчика попыток аутентификации для конкретного логина
    void increaseLoggingCounter(QString login);

    QString         pathToAuthFile;
    std::ifstream   authFStream;
    QMap<QString, int>  loggingCounter;

    UserMod getUserMod(const QString data, const int startIndex);
};

#endif // AUTHENTICATOR_H
